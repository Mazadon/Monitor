# Simple monitoring environment.

Monitoring host metrics using Prometheus and Grafana.

## Installation:

1. Download sources
2. Go to project directory
2. Run in terminal: 
    > vagrant up
3. Open link in browser: 
    > http://localhost:3000/
    
    *login:* admin  
    *password:* admin
